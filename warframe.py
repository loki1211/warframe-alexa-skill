import datetime
import requests
import json
import alexa_helper

def formatTime(timeDelta):
    seconds = timeDelta.seconds
    print ("seconds ", seconds)
    response = ""
    minutes = seconds // 60
    seconds = seconds - (minutes * 60)
    hours = minutes // 60
    minutes = minutes - (hours * 60)
    #print("hours ", hours, " minutes ", minutes)

    if hours > 0:
        if hours > 1:
            response = str(hours) + " hours "
        else:
            response = str(hours) + " hour "

    if minutes > 0:
        if minutes > 1:
            response = response + str(minutes) + " minutes "
        else:
            response = response + str(minutes) + " minute "
    return response

def cetusTime():
    resp = requests.get('http://content.warframe.com/dynamic/worldState.php')
    worldState = json.loads(resp.text)

    cetusRecord=worldState
    for state in worldState['SyndicateMissions']:
        if "CetusSyndicate" in json.dumps(state):
            cetusRecord=state

    testv = int(cetusRecord['Activation']['$date']['$numberLong'])
    startDT = datetime.datetime.fromtimestamp(testv / 1e3)

    testv = int(cetusRecord['Expiry']['$date']['$numberLong'])
    endDT = datetime.datetime.fromtimestamp(testv / 1e3)

    currTime = datetime.datetime.today()
    #print("End: ", endDT)
    #print("Current: ", currTime)
    #print("Start: ", startDT)

    #print("Length: ", endDT - startDT)

    endDelta = endDT - currTime
    startDelta = startDT - currTime

    d = datetime.timedelta(minutes=50)
    d2 = datetime.timedelta(minutes=107)
    d3 = datetime.timedelta(minutes=7)

    if (currTime <= endDT and currTime >= endDT - d):
        return "Currently it is night in cetus with " + formatTime(endDelta + d3) + " remaining."
    else:
        return "Currently it is day in cetus with " + formatTime(startDelta + d2) + " remaining."

def lambda_handler(event, context):
    #print(event)

    if (event['request']['type'] == 'LaunchRequest'):
        return launchRequest(event, context)
    elif event['request']['type'] == "IntentRequest":
        return intentRouter(event, context)

def launchRequest(event, context):
    welcomeMessage = "Welcome to my warframe skill.  Ask me what time is it in cetus."
    return alexa_helper.AlexaHelper.statement("warframe",welcomeMessage)

def intentRouter(event, context):
    intent = event['request']['intent']['name']

    # Custom Intents

    if intent == "LocTime":
        return alexa_helper.AlexaHelper.statement("warframe",cetusTime())

    return launchRequest(event, context)
