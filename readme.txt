This is a simple alexa skill.  It pulls the current web state from 
http://content.warframe.com/dynamic/worldState.php, and extracts the 
current start and end time for the cetus day / night cycle.  This isn't 
a very original work, I used code from these sites:

Sample functions to build responses in python:
https://medium.com/@jasonrigden/how-to-make-an-alexa-skill-with-python-cb8a6a6c4d85

Deathsnacks Plains of Edilon Tracker source:
https://gist.github.com/Wursthandschuh/51319408550abe79ae8c154a6987059f
