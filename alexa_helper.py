#source: https://medium.com/@jasonrigden/how-to-make-an-alexa-skill-with-python-cb8a6a6c4d85
class AlexaHelper:

    def build_PlainSpeech(body):
        speech = {}
        speech['type'] = 'PlainText'
        speech['text'] = body
        return speech

    def build_SimpleCard(title, body):
        card = {}
        card['type'] = 'Simple'
        card['title'] = title
        card['content'] = body
        return card

    def build_response(message, session_attributes={}):
        response = {}
        response['version'] = '1.0'
        response['sessionAttributes'] = session_attributes
        response['response'] = message
        return response

    def statement(title, body):
        speechlet = {}
        speechlet['outputSpeech'] = AlexaHelper.build_PlainSpeech(body)
        speechlet['card'] = AlexaHelper.build_SimpleCard(title, body)
        speechlet['shouldEndSession'] = True
        return AlexaHelper.build_response(speechlet)
